/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */
#include <iostream>
using namespace std;
#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	int bytnumb = 0, wordnumb = 0, linenumb = 0, uwordnumb = 0, maxlen = 0;
	int state = 0;
	int curlen = 0;

	char input;
	string uword;
	string byte;
	set<string> uwordcon;

	enum state {ws,nws};

	while(fread(&input,1,1,stdin))
	{
		bytnumb++;
		if(input == '\t')
			curlen += 8-curlen%8;
		else
			curlen++;

		if(input == ' ')
		{
			if(state == nws){
				uwordcon.insert(uword);
				uword.clear();
				}
				state = ws;
		}
		else if (input == '\t')
			{
				if(state == nws)
				{
					uwordcon.insert(uword);
					uword.clear();
					}
					state = ws;
				}
		else if (input == '\n')
			{
				if(state == nws)
				{
					uwordcon.insert(uword);
					uword.clear();
					}
				if(curlen > maxlen)
					maxlen = curlen-1;
					curlen = 0;
					state = ws;
					linenumb++;
			}
			else {
				if(state == ws)
				{
					wordnumb++;
					}
					state = nws;
					uword.push_back(input);
				}
		uwordnumb = uwordcon.size();
	}

		if(input != '\n'){
			if(curlen > maxlen)
				maxlen = curlen;
		}

	if(linesonly) cout<<linenumb<<"\t";
	if(wordsonly) cout<<wordnumb<<"\t";
	if(charonly) cout<<bytnumb<<"\t";
	if(longonly) cout<<maxlen<<"\t";
	if(uwordsonly) cout<<uwordnumb<<endl;
	if(uwordsonly == 0 && longonly == 0 && charonly == 0 && wordsonly == 0 && linesonly == 0)
		cout<<linenumb<<"\t"<<wordnumb<<"\t"<<bytnumb<<endl;
	return 0;
}